/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Cargo;
import com.example.TesisRA.repository.CargoRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class CargoController {
    
    @Autowired
    CargoRepository cargoRepository;

    //Obtener todos los cargos
    @GetMapping("/cargos")
    public List<Cargo> getAllCargos() {
        return cargoRepository.findAll();
    }

    // Crear un nuevo cargo
    @PostMapping("/cargos")
    public Cargo createCargo(@Valid @RequestBody Cargo cargo) {
        return cargoRepository.save(cargo);
    }

    // Obtener un cargo unico
    @GetMapping("/cargos/{id}")
    public Cargo getCargoById(@PathVariable(value = "id") Long cargoId) {
        return cargoRepository.findById(cargoId)
                .orElseThrow(() -> new ResourceNotFoundException("Cargo", "id", cargoId));
    }

    // Editar un cargo
    @PutMapping("/cargos/{id}")
    public Cargo updateCargo(@PathVariable(value = "id") Long cargoId,
            @Valid @RequestBody Cargo cargoDetails) {
        
        Cargo cargo = cargoRepository.findById(cargoId)
                .orElseThrow(() -> new ResourceNotFoundException("Cargo", "id", cargoId));
        
        cargo.setNombre(cargoDetails.getNombre());
        cargo.setDesignaPersonal(cargoDetails.getDesignaPersonal());
        
        Cargo cargoEditado = cargoRepository.save(cargo);
        return cargoEditado;
    }

    // Eliminar un cargo
    @DeleteMapping("/cargos/{id}")
    public ResponseEntity<?> deleteCargo(@PathVariable(value = "id") Long cargoId) {
        Cargo cargo = cargoRepository.findById(cargoId)
                .orElseThrow(() -> new ResourceNotFoundException("Cargo", "id", cargoId));
        
        cargoRepository.delete(cargo);
        
        return ResponseEntity.ok().build();
    }
}
