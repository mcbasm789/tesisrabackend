/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Horario;
import com.example.TesisRA.repository.HorarioRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class HorarioController {
    
    @Autowired
    HorarioRepository horarioRepository;

    //Obtener todos los horarios
    @GetMapping("/horarios")
    public List<Horario> getAllHorarios() {
        return horarioRepository.findAll(new Sort(Sort.Direction.ASC, "horaInicio"));
    }

    //Obtener todos los horarios
    @PostMapping("/horarios/dia")
    public List<Horario> getHorariosByDia(@Valid @RequestBody String dia) {
        return horarioRepository.findByDia(dia);
    }

    // Crear un nuevo horario
    @PostMapping("/horarios")
    public Horario createHorario(@Valid @RequestBody Horario horario) {
        return horarioRepository.save(horario);
    }

    // Obtener un horario unico
    @GetMapping("/horarios/{id}")
    public Horario getHorarioById(@PathVariable(value = "id") Long horarioId) {
        return horarioRepository.findById(horarioId)
                .orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horarioId));
    }

    // Editar un horario
    @PutMapping("/horarios/{id}")
    public Horario updateHorario(@PathVariable(value = "id") Long horarioId,
            @Valid @RequestBody Horario horarioDetails) {
        
        Horario horario = horarioRepository.findById(horarioId)
                .orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horarioId));
        
        horario.setHoraFin(horarioDetails.getHoraFin());
        horario.setHoraInicio(horarioDetails.getHoraInicio());
        horario.setCursos(horarioDetails.getCursos());
        horario.setDia(horarioDetails.getDia());
        horario.setNroDia(horarioDetails.getNroDia());
        
        Horario horarioEditado = horarioRepository.save(horario);
        return horarioEditado;
    }

    // Eliminar un horario
    @DeleteMapping("/horarios/{id}")
    public ResponseEntity<?> deleteHorario(@PathVariable(value = "id") Long horarioId) {
        Horario horario = horarioRepository.findById(horarioId)
                .orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horarioId));
        
        horarioRepository.delete(horario);
        
        return ResponseEntity.ok().build();
    }
}
