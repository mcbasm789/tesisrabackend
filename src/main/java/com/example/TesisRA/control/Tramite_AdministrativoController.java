/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Tramite_Administrativo;
import com.example.TesisRA.repository.Tramite_AdministrativoRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class Tramite_AdministrativoController {

    @Autowired
    Tramite_AdministrativoRepository tramiteAdministrativoRepository;

    //Obtener todos los tramiteAdministrativos
    @GetMapping("/tramiteAdministrativos")
    public List<Tramite_Administrativo> getAllTramite_Administrativos() {
        return tramiteAdministrativoRepository.findAll();
    }

    // Crear un nuevo tramiteAdministrativo
    @PostMapping("/tramiteAdministrativos")
    public Tramite_Administrativo createTramite_Administrativo(@Valid @RequestBody Tramite_Administrativo tramiteAdministrativo) {
        return tramiteAdministrativoRepository.save(tramiteAdministrativo);
    }

    // Obtener un tramiteAdministrativo unico
    @GetMapping("/tramiteAdministrativos/{id}")
    public Tramite_Administrativo getTramite_AdministrativoById(@PathVariable(value = "id") Long tramiteAdministrativoId) {
        return tramiteAdministrativoRepository.findById(tramiteAdministrativoId)
                .orElseThrow(() -> new ResourceNotFoundException("Tramite_Administrativo", "id", tramiteAdministrativoId));
    }

    // Editar un tramiteAdministrativo
    @PutMapping("/tramiteAdministrativos/{id}")
    public Tramite_Administrativo updateTramite_Administrativo(@PathVariable(value = "id") Long tramiteAdministrativoId,
            @Valid @RequestBody Tramite_Administrativo tramiteAdministrativoDetails) {

        Tramite_Administrativo tramiteAdministrativo = tramiteAdministrativoRepository.findById(tramiteAdministrativoId)
                .orElseThrow(() -> new ResourceNotFoundException("Tramite_Administrativo", "id  ", tramiteAdministrativoId));

        tramiteAdministrativo.setDescripcion(tramiteAdministrativoDetails.getDescripcion());
        tramiteAdministrativo.setCodigo(tramiteAdministrativoDetails.getCodigo());
        tramiteAdministrativo.setNombre(tramiteAdministrativoDetails.getNombre());
        tramiteAdministrativo.setEsRealizadaOficinas(tramiteAdministrativoDetails.getEsRealizadaOficinas());

        Tramite_Administrativo tramiteAdministrativoEditado = tramiteAdministrativoRepository.save(tramiteAdministrativo);
        return tramiteAdministrativoEditado;
    }

    // Eliminar un tramiteAdministrativo
    @DeleteMapping("/tramiteAdministrativos/{id}")
    public ResponseEntity<?> deleteTramite_Administrativo(@PathVariable(value = "id") Long tramiteAdministrativoId) {
        Tramite_Administrativo tramiteAdministrativo = tramiteAdministrativoRepository.findById(tramiteAdministrativoId)
                .orElseThrow(() -> new ResourceNotFoundException("Tramite_Administrativo", "id", tramiteAdministrativoId));

        tramiteAdministrativoRepository.delete(tramiteAdministrativo);

        return ResponseEntity.ok().build();
    }
}
