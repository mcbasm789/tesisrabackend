package com.example.TesisRA.control;


import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Alumno;
import com.example.TesisRA.modelo.Ambiente;
import com.example.TesisRA.modelo.Curso;
import com.example.TesisRA.modelo.Personal_Administrativo;
import com.example.TesisRA.repository.AmbienteRepository;
import com.example.TesisRA.repository.Personal_AdministrativoRepository;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class AmbientePersonalController {

    @Autowired
    Personal_AdministrativoRepository personalAdministrativoRepository;

    @Autowired
    AmbienteRepository ambienteRepository;

    // Asginar el personal
    @PostMapping("/asignarPersonal")
    public Ambiente asignarPersonal(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject ambienteJSON = body.getJSONObject(0);
        JSONArray personalJSON = (JSONArray) body.get(1);
        //Lista de personal a editar
        List<Personal_Administrativo> personalContratado = new ArrayList<>();
        //Obtener el ambiente desde el JSON
        Ambiente ambiente = ambienteRepository.findById(ambienteJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteJSON.getLong("id")));
        //Verificar que exista el ambiente
        if (ambiente.getId() != null) //Barrer la lista de alumnos y asignarla
        {
            for (int i = 0; i < personalJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject personal = personalJSON.getJSONObject(i);
                //Encontrar el personal en la base de datos
                Personal_Administrativo personalEnlazado = personalAdministrativoRepository.findById(personal.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Personal Administrativo", "id", personal.getLong("id")));
                //Verificiar que exista el alumno
                if (personalEnlazado.getId() != null) {
                    //Verificar que el personal no este en el ambiente
                    Boolean controlaPersonal = false;
                    //Si la lista no existe crearla
                    if (ambiente.getGestionaPersonal() == null) {
                        ambiente.setGestionaPersonal(new ArrayList<>());
                    } else {
                        for (Personal_Administrativo personalActual : ambiente.getGestionaPersonal()) {
                            if (personalActual == personalEnlazado) {
                                controlaPersonal = true;
                                break;
                            }
                        }
                    }
                    //Si el personal no trabaja en el ambiente añadirlo
                    if (!controlaPersonal) {
                        ambiente.getGestionaPersonal().add(personalEnlazado);
                    }

                    //Asignar el ambiente al personal
                    personalEnlazado.setAmbiente(ambiente);
                    //Añadir el personal a la lista de ediciones
                    personalContratado.add(personalEnlazado);

                }
            }

            //Editar el ambiente y el personal;
            ambienteRepository.save(ambiente);
            personalContratado.forEach((personalNuevo) -> {
                personalAdministrativoRepository.save(personalNuevo);
            });
        }
        return ambiente;
    }
}
