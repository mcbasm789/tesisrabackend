/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Edificio;
import com.example.TesisRA.repository.EdificioRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class EdificioController {

    @Autowired
    EdificioRepository edificioRepository;

    //Obtener todos los edificios
    @GetMapping("/edificios")
    public List<Edificio> getAllEdificios() {
        return edificioRepository.findAll();
    }

    // Crear un nuevo edificio
    @PostMapping("/edificios")
    public Edificio createEdificio(@Valid @RequestBody Edificio edificio) {
        return edificioRepository.save(edificio);
    }

    // Obtener un edificio unico
    @GetMapping("/edificios/{id}")
    public Edificio getEdificioById(@PathVariable(value = "id") Long edificioId) {
        return edificioRepository.findById(edificioId)
                .orElseThrow(() -> new ResourceNotFoundException("Edificio", "id", edificioId));
    }

    // Editar un edificio
    @PutMapping("/edificios/{id}")
    public Edificio updateEdifcio(@PathVariable(value = "id") Long edificioId,
            @Valid @RequestBody Edificio edificioDetails) {

        Edificio edificio = edificioRepository.findById(edificioId)
                .orElseThrow(() -> new ResourceNotFoundException("Edificio", "id", edificioId));

        edificio.setNroPisos(edificioDetails.getNroPisos());
        edificio.setNombre(edificioDetails.getNombre());
        edificio.setContieneAmbientes(edificioDetails.getContieneAmbientes());

        Edificio edificioEditado = edificioRepository.save(edificio);
        return edificioEditado;
    }

    // Eliminar un edificio
    @DeleteMapping("/edificios/{id}")
    public ResponseEntity<?> deleteEdificio(@PathVariable(value = "id") Long edificioId) {
        Edificio edificio = edificioRepository.findById(edificioId)
                .orElseThrow(() -> new ResourceNotFoundException("Edificio", "id", edificioId));

        edificioRepository.delete(edificio);

        return ResponseEntity.ok().build();
    }
}
