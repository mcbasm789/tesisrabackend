/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Ambiente;
import com.example.TesisRA.modelo.Edificio;
import com.example.TesisRA.modelo.Tipo_Ambiente;
import com.example.TesisRA.repository.AmbienteRepository;
import com.example.TesisRA.repository.EdificioRepository;
import com.example.TesisRA.repository.Tipo_AmbienteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class AmbienteController {
    
    @Autowired
    AmbienteRepository ambienteRepository;
    @Autowired
    Tipo_AmbienteRepository tipoAmbienteRepository;
    @Autowired
    EdificioRepository edificioRepository;
    @Autowired
    private ObjectMapper objectMapper;
    
    @PostConstruct
    public void setUp() {
        objectMapper.registerModule(new JavaTimeModule());
    }

    //Obtener todos los ambientes
    @GetMapping("/ambientes")
    public List<Ambiente> getAllAmbientes() {
        return ambienteRepository.findAll();
    }

    //Obtener todos los ambientes que son oficinas
    @GetMapping("/ambientes/oficinas")
    public List<Ambiente> getAllAmbientesOficinas() {
        //Obtener el tipo de ambiente
        Tipo_Ambiente tipoAmbiente = tipoAmbienteRepository.findByNombre("Oficina Administrativa");
        if (tipoAmbiente.getId() != null) {
            //Obtener los ambiente
            return ambienteRepository.findByTipo(tipoAmbiente);
        }
        return null;
    }

    //Obtener todos los ambientes que son aulas
    @GetMapping("/ambientes/aulas")
    public List<Ambiente> getAllAmbientesAulas() {
        //Obtener el tipo de ambiente
        Tipo_Ambiente tipoAmbiente = tipoAmbienteRepository.findByNombre("Aula");
        if (tipoAmbiente.getId() != null) {
            //Obtener los ambiente
            return ambienteRepository.findByTipo(tipoAmbiente);
        }
        return null;
    }

    //Obtener todos los ambientes que no son aulas
    @GetMapping("/ambientes/noAulas")
    public List<Ambiente> getAllAmbientesNoAulas() {
        //Obtener el tipo de ambiente
        Tipo_Ambiente tipoAmbiente = tipoAmbienteRepository.findByNombre("Aula");
        if (tipoAmbiente.getId() != null) {
            //Obtener los ambiente
            return ambienteRepository.findByNotTipo(tipoAmbiente);
        }
        return null;
    }

    //Obtener todos los ambientes que no son oficinas Administrativas
    @GetMapping("/ambientes/noOficinas")
    public List<Ambiente> getAllAmbientesNoOficinas() {
        //Obtener el tipo de ambiente
        Tipo_Ambiente tipoAmbiente = tipoAmbienteRepository.findByNombre("Oficina Administrativa");
        if (tipoAmbiente.getId() != null) {
            //Obtener los ambiente
            return ambienteRepository.findByNotTipo(tipoAmbiente);
        }
        return null;
    }

    //Obtener un ambiente por su idVuforia
    @GetMapping("/ambientes/idVuforia/{id}")
    public Ambiente getAmbienteByIdVuforia(@PathVariable(value = "id") String idVuforia) {
        //Obtener los ambiente
        return ambienteRepository.findByIdVuforia(idVuforia);
    }

    // Crear un nuevo ambiente
    @PostMapping("/ambientes")
    public Ambiente createAmbiente(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject ambienteJSON = body.getJSONObject(0);
        JSONObject tipoAmbienteJSON = body.getJSONObject(1);
        JSONObject edificioJSON = body.getJSONObject(2);

        //Obtener el tipoAmbiente y el Edificio
        Edificio edificio = edificioRepository.findById(edificioJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Edificio", "id", edificioJSON.getLong("id")));
        Tipo_Ambiente tipoAmbiente = tipoAmbienteRepository.findById(tipoAmbienteJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Tipo Ambiente", "id", edificioJSON.getLong("id")));
        //Verificar la existencia de la lista de ambientes
        if (edificio.getId() != null && tipoAmbiente.getId() != null) {
            try {
                //Si no existe crearla
                if (edificio.getContieneAmbientes() == null) {
                    edificio.setContieneAmbientes(new ArrayList<Ambiente>());
                }
                if (tipoAmbiente.getRegistraAmbientes() == null) {
                    tipoAmbiente.setRegistraAmbientes(new ArrayList<Ambiente>());
                }
                //Crear el ambiente desde el JSON
                Ambiente ambiente = objectMapper.readValue(ambienteJSON.toString(), Ambiente.class);
                //Asignar el tipoAmbiente y el edificio
                ambiente.setEdificio(edificio);
                ambiente.setTipoAmbiente(tipoAmbiente);
                //Crearla en base de datos
                Ambiente ambienteCreado = ambienteRepository.save(ambiente);

                //Asignar el ambiente al tipo y edificio
                edificio.getContieneAmbientes().add(ambienteCreado);
                tipoAmbiente.getRegistraAmbientes().add(ambienteCreado);
                //Editar el tipo de ambiente y el edificio
                edificioRepository.save(edificio);
                tipoAmbienteRepository.save(tipoAmbiente);
                return ambienteCreado;
            } catch (IOException ex) {
                Logger.getLogger(CursoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    // Obtener un ambiente unico
    @GetMapping("/ambientes/{id}")
    public Ambiente getAmbienteById(@PathVariable(value = "id") Long ambienteId) {
        return ambienteRepository.findById(ambienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteId));
    }

    // Editar un ambiente
    @PutMapping("/ambientes/{id}")
    public Ambiente updateAmbiente(@PathVariable(value = "id") Long ambienteId,
            @Valid @RequestBody Object[] requestBody) {
        Ambiente ambiente = ambienteRepository.findById(ambienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteId));
        
        if (ambiente.getId() != null) {
            //Dividir el arreglo
            JSONArray body = new JSONArray(requestBody);
            JSONObject ambienteJSON = body.getJSONObject(0);
            JSONObject tipoAmbienteJSON = body.getJSONObject(1);
            JSONObject edificioJSON = body.getJSONObject(2);

            //Obtener el tipoAmbiente y el Edificio
            Edificio edificio = edificioRepository.findById(edificioJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Edificio", "id", edificioJSON.getLong("id")));
            Tipo_Ambiente tipoAmbiente = tipoAmbienteRepository.findById(tipoAmbienteJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Tipo Ambiente", "id", edificioJSON.getLong("id")));
            //Verificar la existencia de la lista de cursos
            if (edificio.getId() != null && tipoAmbiente.getId() != null) {
                try {
                    //Crear el ambientel desde el JSON
                    Ambiente ambienteDetails = objectMapper.readValue(ambienteJSON.toString(), Ambiente.class);
                    //Asignar la facultad
                    ambiente.setCodigo(ambienteDetails.getCodigo());
                    ambiente.setNombre(ambienteDetails.getNombre());
                    ambiente.setAforo(ambienteDetails.getAforo());
                    ambiente.setIdVuforia(ambienteDetails.getIdVuforia());
                    ambiente.setUrlImagen(ambienteDetails.getUrlImagen());
                    ambiente.setNroPiso(ambienteDetails.getNroPiso());
                    ambiente.setEdificio(ambienteDetails.getEdificio());
                    ambiente.setTipoAmbiente(ambienteDetails.getTipoAmbiente());
                    ambiente.setEsUsadoCursos(ambienteDetails.getEsUsadoCursos());
                    ambiente.setGestionaPersonal(ambiente.getGestionaPersonal());
                    ambiente.setRealizaTramites(ambienteDetails.getRealizaTramites());
                    ambiente.setEdificio(edificio);
                    ambiente.setTipoAmbiente(tipoAmbiente);
                    
                    Ambiente ambienteEditado = ambienteRepository.save(ambiente);

                    //Verificar si el edificio tiene ese ambiente
                    Boolean edificioAmbiente = false;

                    //Verificar la existencia de la lista de ambientes
                    if (edificio.getContieneAmbientes() == null) {
                        //Si no existe crearla
                        edificio.setContieneAmbientes(new ArrayList<Ambiente>());
                    } else {
                        //Verificar que la escuela no este en la facultad
                        for (Ambiente ambienteContenido : edificio.getContieneAmbientes()) {
                            if (ambienteContenido == ambienteEditado) {
                                edificioAmbiente = true;
                                break;
                            }
                        }
                    }
                    //Asignar el ambiente creado al edificio (si es necesario)
                    if (!edificioAmbiente) {
                        edificio.getContieneAmbientes().add(ambienteEditado);
                        //Editar la facultad
                        edificioRepository.save(edificio);
                    }

                    //Verificar si el edificio tiene ese ambiente
                    Boolean tipoAmbienteRegistrado = false;

                    //Verificar la existencia de la lista de ambientes
                    if (tipoAmbiente.getRegistraAmbientes() == null) {
                        //Si no existe crearla
                        tipoAmbiente.setRegistraAmbientes(new ArrayList<Ambiente>());
                    } else {
                        //Verificar que la escuela no este en la facultad
                        for (Ambiente ambienteContenido : tipoAmbiente.getRegistraAmbientes()) {
                            if (ambienteContenido == ambienteEditado) {
                                tipoAmbienteRegistrado = true;
                                break;
                            }
                        }
                    }
                    //Asignar el ambiente creado al edificio (si es necesario)
                    if (!tipoAmbienteRegistrado) {
                        tipoAmbiente.getRegistraAmbientes().add(ambienteEditado);
                        //Editar la facultad
                        tipoAmbienteRepository.save(tipoAmbiente);
                    }
                    
                    return ambienteEditado;
                } catch (IOException ex) {
                    Logger.getLogger(CursoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    // Eliminar un ambiente
    @DeleteMapping("/ambientes/{id}")
    public ResponseEntity<?> deleteAmbiente(@PathVariable(value = "id") Long ambienteId) {
        Ambiente ambiente = ambienteRepository.findById(ambienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteId));
        
        ambienteRepository.delete(ambiente);
        
        return ResponseEntity.ok().build();
    }
}
