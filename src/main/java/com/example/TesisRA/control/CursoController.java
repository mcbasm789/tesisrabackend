/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Curso;
import com.example.TesisRA.modelo.Escuela_Profesional;
import com.example.TesisRA.modelo.Facultad;
import com.example.TesisRA.repository.CursoRepository;
import com.example.TesisRA.repository.Escuela_ProfesionalRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class CursoController {

    @Autowired
    CursoRepository cursoRepository;
    @Autowired
    Escuela_ProfesionalRepository escuelaProfesionalRepository;

    //Obtener todos los cursos
    @GetMapping("/cursos")
    public List<Curso> getAllCursos() {
        return cursoRepository.findAll();
    }

    // Crear un nuevo curso
    @PostMapping("/cursos")
    public Curso createCurso(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject cursoJSON = body.getJSONObject(0);
        JSONObject escuelaProfesionalJSON = body.getJSONObject(1);

        //Obtener la escuelaProfesional
        Escuela_Profesional escuelaProfesional = escuelaProfesionalRepository.findById(escuelaProfesionalJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Escuela Profesional", "id", escuelaProfesionalJSON.getLong("id")));
        //Verificar la existencia de la lista de cursos
        if (escuelaProfesional.getId() != null) {
            try {
                //Si no existe crearla
                if (escuelaProfesional.getGestionaCursos() == null) {
                    escuelaProfesional.setGestionaCursos(new ArrayList<Curso>());
                }
                //Crear el curso desde el JSON
                Curso curso = new ObjectMapper().readValue(cursoJSON.toString(), Curso.class);
                //Asignar la escuela
                curso.setEscuelaProfesional(escuelaProfesional);
                //Crearla en base de datos
                Curso cursoCreado = cursoRepository.save(curso);

                //Asignar la escuela creada a la facultad
                escuelaProfesional.getGestionaCursos().add(cursoCreado);
                //Editar la escuela
                escuelaProfesionalRepository.save(escuelaProfesional);
                return cursoCreado;
            } catch (IOException ex) {
                Logger.getLogger(CursoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    // Obtener un curso unico
    @GetMapping("/cursos/{id}")
    public Curso getCursoById(@PathVariable(value = "id") Long cursoId) {
        return cursoRepository.findById(cursoId)
                .orElseThrow(() -> new ResourceNotFoundException("Curso", "id", cursoId));
    }

    // Editar un curso
    @PutMapping("/cursos/{id}")
    public Curso updateCurso(@PathVariable(value = "id") Long cursoId,
            @Valid @RequestBody Object[] requestBody) {

        Curso curso = cursoRepository.findById(cursoId)
                .orElseThrow(() -> new ResourceNotFoundException("Curso", "id", cursoId));

        if (curso.getId() != null) {
            //Dividir el arreglo
            JSONArray body = new JSONArray(requestBody);
            JSONObject cursoJSON = body.getJSONObject(0);
            JSONObject escuelaProfesionalJSON = body.getJSONObject(1);

            //Obtener la escuelaProfesional
            Escuela_Profesional escuelaProfesional = escuelaProfesionalRepository.findById(escuelaProfesionalJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Escuela Profesional", "id", escuelaProfesionalJSON.getLong("id")));
            //Verificar la existencia de la lista de cursos
            if (escuelaProfesional.getId() != null) {
                try {
                    //Crear la escuela profesional desde el JSON
                    Curso cursoDetails = new ObjectMapper().readValue(cursoJSON.toString(), Curso.class);
                    //Asignar la facultad
                    curso.setCodigo(cursoDetails.getCodigo());
                    curso.setNombre(cursoDetails.getNombre());
                    curso.setEsDictadoAmbientes(cursoDetails.getEsDictadoAmbientes());
                    curso.setEsDictadoDocentes(cursoDetails.getEsDictadoDocentes());
                    curso.setEsLlevadoAlumnos(cursoDetails.getEsLlevadoAlumnos());
                    curso.setEscuelaProfesional(escuelaProfesional);

                    Curso cursoEditado = cursoRepository.save(curso);

                    //Verificar si la facultad ya gestiona dicha escuela
                    Boolean gestionaCurso = false;

                    //Verificar la existencia de la lista de escuelas
                    if (escuelaProfesional.getGestionaCursos() == null) {
                        //Si no existe crearla
                        escuelaProfesional.setGestionaCursos(new ArrayList<Curso>());
                    } else {
                        //Verificar que la escuela no este en la facultad
                        for (Curso cursoGestionado : escuelaProfesional.getGestionaCursos()) {
                            if (cursoGestionado == cursoEditado) {
                                gestionaCurso = true;
                                break;
                            }
                        }
                    }
                    //Asignar la escuela creada a la facultad (si es necesario)
                    if (!gestionaCurso) {
                        escuelaProfesional.getGestionaCursos().add(cursoEditado);
                        //Editar la facultad
                        escuelaProfesionalRepository.save(escuelaProfesional);
                    }
                    return cursoEditado;
                } catch (IOException ex) {
                    Logger.getLogger(CursoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    // Eliminar un curso
    @DeleteMapping("/cursos/{id}")
    public ResponseEntity<?> deleteCurso(@PathVariable(value = "id") Long cursoId) {
        Curso curso = cursoRepository.findById(cursoId)
                .orElseThrow(() -> new ResourceNotFoundException("Curso", "id", cursoId));

        cursoRepository.delete(curso);

        return ResponseEntity.ok().build();
    }
}
