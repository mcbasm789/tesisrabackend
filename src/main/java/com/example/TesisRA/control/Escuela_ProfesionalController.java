/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Escuela_Profesional;
import com.example.TesisRA.modelo.Facultad;
import com.example.TesisRA.repository.Escuela_ProfesionalRepository;
import com.example.TesisRA.repository.FacultadRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class Escuela_ProfesionalController {
    
    @Autowired
    Escuela_ProfesionalRepository escuelaProfesionalRepository;
    @Autowired
    FacultadRepository facultadRepository;

    //Obtener todos los escuelaProfesionals
    @GetMapping("/escuelaProfesional")
    public List<Escuela_Profesional> getAllEscuelaProfesionals() {
        return escuelaProfesionalRepository.findAll();
    }

    // Crear un nuevo escuelaProfesional
    @PostMapping("/escuelaProfesional")
    public Escuela_Profesional createEscuelaProfesional(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject escuelaProfesionalJSON = body.getJSONObject(0);
        JSONObject facultadJSON = body.getJSONObject(1);

        //Obtener la facultad
        Facultad facultad = facultadRepository.findById(facultadJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Facultad", "id", facultadJSON.getLong("id")));
        //Verificar la existencia de la lista de escuelas
        if (facultad.getId() != null) {
            try {
                //Si no existe crearla
                if (facultad.getGestionaEscuelas() == null) {
                    facultad.setGestionaEscuelas(new ArrayList<Escuela_Profesional>());
                }
                //Crear la escuela profesional desde el JSON
                Escuela_Profesional escuelaProfesional = new ObjectMapper().readValue(escuelaProfesionalJSON.toString(), Escuela_Profesional.class);
                //Asignar la facultad
                escuelaProfesional.setFacultad(facultad);
                //Crearla en base de datos
                Escuela_Profesional epCreada = escuelaProfesionalRepository.save(escuelaProfesional);

                //Asignar la escuela creada a la facultad
                facultad.getGestionaEscuelas().add(epCreada);
                //Editar la facultad
                facultadRepository.save(facultad);
                return epCreada;
            } catch (IOException ex) {
                Logger.getLogger(Escuela_ProfesionalController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    // Obtener un escuelaProfesional unico
    @GetMapping("/escuelaProfesional/{id}")
    public Escuela_Profesional getEscuelaProfesionalById(@PathVariable(value = "id") Long escuelaProfesionalId) {
        return escuelaProfesionalRepository.findById(escuelaProfesionalId)
                .orElseThrow(() -> new ResourceNotFoundException("Escuela_Profesional", "id", escuelaProfesionalId));
    }

    // Editar un escuelaProfesional
    @PutMapping("/escuelaProfesional/{id}")
    public Escuela_Profesional updateEscuelaProfesional(@PathVariable(value = "id") Long escuelaProfesionalId,
            @Valid @RequestBody Object[] requestBody) {
        
        Escuela_Profesional escuelaProfesional = escuelaProfesionalRepository.findById(escuelaProfesionalId)
                .orElseThrow(() -> new ResourceNotFoundException("Escuela_Profesional", "id", escuelaProfesionalId));
        
        if (escuelaProfesional.getId() != null) {
            //Dividir el arreglo
            JSONArray body = new JSONArray(requestBody);
            JSONObject escuelaProfesionalJSON = body.getJSONObject(0);
            JSONObject facultadJSON = body.getJSONObject(1);

            //Obtener la facultad
            Facultad facultad = facultadRepository.findById(facultadJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Facultad", "id", facultadJSON.getLong("id")));
            if (facultad.getId() != null) {
                try {

                    //Crear la escuela profesional desde el JSON
                    Escuela_Profesional escuelaProfesionalDetails = new ObjectMapper().readValue(escuelaProfesionalJSON.toString(), Escuela_Profesional.class);
                    //Asignar la facultad
                    escuelaProfesional.setFacultad(facultad);
                    escuelaProfesional.setFacultad(escuelaProfesionalDetails.getFacultad());
                    escuelaProfesional.setNombre(escuelaProfesionalDetails.getNombre());
                    escuelaProfesional.setGestionaCursos(escuelaProfesionalDetails.getGestionaCursos());
                    escuelaProfesional.setFacultad(facultad);
                    
                    Escuela_Profesional escuelaProfesionalEditado = escuelaProfesionalRepository.save(escuelaProfesional);

                    //Verificar si la facultad ya gestiona dicha escuela
                    Boolean gestionaEscuela = false;

                    //Verificar la existencia de la lista de escuelas
                    if (facultad.getGestionaEscuelas() == null) {
                        //Si no existe crearla
                        facultad.setGestionaEscuelas(new ArrayList<Escuela_Profesional>());
                    } else {
                        //Verificar que la escuela no este en la facultad
                        for (Escuela_Profesional escuela : facultad.getGestionaEscuelas()) {
                            if (escuela == escuelaProfesionalEditado) {
                                gestionaEscuela = true;
                                break;
                            }
                        }
                    }
                    //Asignar la escuela creada a la facultad (si es necesario)
                    if (!gestionaEscuela) {
                        facultad.getGestionaEscuelas().add(escuelaProfesionalEditado);
                        //Editar la facultad
                        facultadRepository.save(facultad);
                    }
                    return escuelaProfesionalEditado;
                } catch (IOException ex) {
                    Logger.getLogger(Escuela_ProfesionalController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    // Eliminar un escuelaProfesional
    @DeleteMapping("/escuelaProfesional/{id}")
    public ResponseEntity<?> deleteEscuelaProfesional(@PathVariable(value = "id") Long escuelaProfesionalId) {
        Escuela_Profesional escuelaProfesional = escuelaProfesionalRepository.findById(escuelaProfesionalId)
                .orElseThrow(() -> new ResourceNotFoundException("Escuela_Profesional", "id", escuelaProfesionalId));
        
        escuelaProfesionalRepository.delete(escuelaProfesional);
        
        return ResponseEntity.ok().build();
    }
}
