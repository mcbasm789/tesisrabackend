/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Ambiente;
import com.example.TesisRA.modelo.Ambiente_Curso;
import com.example.TesisRA.modelo.Curso;
import com.example.TesisRA.modelo.Horario;
import com.example.TesisRA.repository.AmbienteRepository;
import com.example.TesisRA.repository.Ambiente_CursoRepository;
import com.example.TesisRA.repository.CursoRepository;
import com.example.TesisRA.repository.HorarioRepository;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class Ambiente_CursoController {

    @Autowired
    Ambiente_CursoRepository ambienteCursoRepository;
    @Autowired
    AmbienteRepository ambienteRepository;
    @Autowired
    CursoRepository cursoRepository;
    @Autowired
    HorarioRepository horarioRepository;

    //Obtener todos los ambienteCursos
    @GetMapping("/ambienteCursos")
    public List<Ambiente_Curso> getAllAmbiente_Cursos() {
        return ambienteCursoRepository.findAll();
    }

    // Crear un nuevo ambienteCurso
    @PostMapping("/ambienteCursos")
    public Ambiente_Curso createAmbiente_Curso(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject ambienteJSON = body.getJSONObject(0);
        JSONObject cursoJSON = body.getJSONObject(1);
        JSONArray horariosJSON = (JSONArray) body.get(2);
        //Lista de horarios a editar
        List<Horario> horariosRegistrados = new ArrayList<>();
        //Obtener el ambiente
        Ambiente ambiente = ambienteRepository.findById(ambienteJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteJSON.getLong("id")));
        //Obtener el curso
        Curso curso = cursoRepository.findById(cursoJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Curso", "id", cursoJSON.getLong("id")));

        //Verificar que el curso y el ambiente existan
        if (ambiente.getId() != null && curso.getId() != null) {
            //Crear el nuevo ambiente curso
            Ambiente_Curso ambienteCurso = new Ambiente_Curso();
            ambienteCurso.setAmbiente(ambiente);
            ambienteCurso.setCurso(curso);
            ambienteCurso.setHorarios(new ArrayList<>());

            //Barrer la lista de horarios y asignarla
            for (int i = 0; i < horariosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject horario = horariosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Horario horarioEnlazado = horarioRepository.findById(horario.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horario.getLong("id")));
                //Verificiar que exista el horario
                if (horarioEnlazado.getId() != null) {
                    //Añadir los horarios al ambiente curso    
                    ambienteCurso.getHorarios().add(horarioEnlazado);
                }
            }

            //Crear el ambienteCurso en base de datos
            Ambiente_Curso ambienteCursoCreado = ambienteCursoRepository.save(ambienteCurso);

            //Asignar en nuevo ambiente curso a cada uno de los objetos iniciales
            //Ambiente
            //Verificar que exista la lista de ambientes curso sino crearla
            if (ambiente.getEsUsadoCursos() == null) {
                ambiente.setEsUsadoCursos(new ArrayList<>());
            }
            ambiente.getEsUsadoCursos().add(ambienteCursoCreado);
            //Guardarlo en base de datos
            ambienteRepository.save(ambiente);

            //Curso
            if (curso.getEsDictadoAmbientes() == null) {
                curso.setEsDictadoAmbientes(new ArrayList<>());
            }
            curso.getEsDictadoAmbientes().add(ambienteCursoCreado);
            //Guardarlo en base de datos
            cursoRepository.save(curso);

            //Horarios
            //Barrer la lista de horarios y asignarla
            for (int i = 0; i < horariosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject horario = horariosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Horario horarioEnlazado = horarioRepository.findById(horario.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horario.getLong("id")));
                //Verificiar que exista el horario
                if (horarioEnlazado.getId() != null) {
                    //Añadir los horarios al ambiente curso    
                    horarioEnlazado.getCursos().add(ambienteCursoCreado);
                    //Guardarlo en base de datos
                    horarioRepository.save(horarioEnlazado);
                }
            }
            return ambienteCursoCreado;
        }
        return null;
    }

    // Obtener un ambienteCurso unico
    @GetMapping("/ambienteCursos/{id}")
    public Ambiente_Curso getAmbiente_CursoById(@PathVariable(value = "id") Long ambienteCursoId) {
        return ambienteCursoRepository.findById(ambienteCursoId)
                .orElseThrow(() -> new ResourceNotFoundException("Ambiente_Curso", "id", ambienteCursoId));
    }

    // Editar un ambienteCurso
    @PutMapping("/ambienteCursos/{id}")
    public Ambiente_Curso updateAmbiente_Curso(@PathVariable(value = "id") Long ambienteCursoId,
            @Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject ambienteJSON = body.getJSONObject(0);
        JSONObject cursoJSON = body.getJSONObject(1);
        JSONArray horariosJSON = (JSONArray) body.get(2);

        Ambiente_Curso ambienteCurso = ambienteCursoRepository.findById(ambienteCursoId)
                .orElseThrow(() -> new ResourceNotFoundException("Ambiente_Curso", "id", ambienteCursoId));

        //Obtener el ambiente
        Ambiente ambiente = ambienteRepository.findById(ambienteJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteJSON.getLong("id")));
        //Obtener el curso
        Curso curso = cursoRepository.findById(cursoJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Curso", "id", cursoJSON.getLong("id")));

        //Verificar que el curso y el ambiente existan
        if (ambienteCurso.getId() != null && ambiente.getId() != null && curso.getId() != null) {
            //Crear el nuevo ambiente curso
            ambienteCurso.setAmbiente(ambiente);
            ambienteCurso.setCurso(curso);

            //Eliminar los cursos de los horarios
            for (Horario horario : ambienteCurso.getHorarios()) {
                horario.getCursos().remove(ambienteCurso);
                horarioRepository.save(horario);
            }
            //reinicializar la lista
            ambienteCurso.setHorarios(new ArrayList<>());

            //Barrer la lista de horarios y asignarla
            for (int i = 0; i < horariosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject horario = horariosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Horario horarioEnlazado = horarioRepository.findById(horario.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horario.getLong("id")));
                //Verificiar que exista el horario
                if (horarioEnlazado.getId() != null) {
                    //Añadir los horarios al ambiente curso    
                    ambienteCurso.getHorarios().add(horarioEnlazado);
                }
            }

            //Editar el ambienteCurso en base de datos
            Ambiente_Curso ambienteCursoEditado = ambienteCursoRepository.save(ambienteCurso);

            //Asignar en nuevo ambiente curso a cada uno de los objetos iniciales
            //Ambiente
            //Verificar que exista la lista de ambientes curso sino crearla
            if (ambiente.getEsUsadoCursos() == null) {
                ambiente.setEsUsadoCursos(new ArrayList<>());
                ambiente.getEsUsadoCursos().add(ambienteCursoEditado);
                //Guardarlo en base de datos
                ambienteRepository.save(ambiente);
            } else {
                //Verificar que no exista en la lista
                if (!ambiente.getEsUsadoCursos().contains(ambienteCursoEditado)) {
                    ambiente.getEsUsadoCursos().add(ambienteCursoEditado);
                    //Guardarlo en base de datos
                    ambienteRepository.save(ambiente);
                }
            }
            //Curso
            if (curso.getEsDictadoAmbientes() == null) {
                curso.setEsDictadoAmbientes(new ArrayList<>());
                curso.getEsDictadoAmbientes().add(ambienteCursoEditado);
                //Guardarlo en base de datos
                cursoRepository.save(curso);
            } else {
                if (!curso.getEsDictadoAmbientes().contains(ambienteCursoEditado)) {
                    curso.getEsDictadoAmbientes().add(ambienteCursoEditado);
                    //Guardarlo en base de datos
                    cursoRepository.save(curso);
                }
            }

            //Horarios
            //Barrer la lista de horarios y asignarla
            for (int i = 0; i < horariosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject horario = horariosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Horario horarioEnlazado = horarioRepository.findById(horario.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horario.getLong("id")));
                //Verificiar que exista el horario
                if (horarioEnlazado.getId() != null) {
                    if (horarioEnlazado.getCursos() != null) {
                        if (!horarioEnlazado.getCursos().contains(ambienteCursoEditado)) {
                            //Añadir los horarios al ambiente curso    
                            horarioEnlazado.getCursos().add(ambienteCursoEditado);
                            //Guardarlo en base de datos
                            horarioRepository.save(horarioEnlazado);
                        }
                    }
                }
            }
            return ambienteCursoEditado;
        }
        return null;
    }

    // Eliminar un ambienteCurso
    @DeleteMapping("/ambienteCursos/{id}")
    public ResponseEntity<?> deleteAmbiente_Curso(@PathVariable(value = "id") Long ambienteCursoId) {
        Ambiente_Curso ambienteCurso = ambienteCursoRepository.findById(ambienteCursoId)
                .orElseThrow(() -> new ResourceNotFoundException("Ambiente_Curso", "id", ambienteCursoId));

        ambienteCursoRepository.delete(ambienteCurso);

        return ResponseEntity.ok().build();
    }
}
