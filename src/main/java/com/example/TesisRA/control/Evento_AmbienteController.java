/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Ambiente;
import com.example.TesisRA.modelo.Evento_Ambiente;
import com.example.TesisRA.modelo.Evento;
import com.example.TesisRA.modelo.Evento;
import com.example.TesisRA.modelo.Evento_Ambiente;
import com.example.TesisRA.modelo.Horario;
import com.example.TesisRA.repository.AmbienteRepository;
import com.example.TesisRA.repository.Evento_AmbienteRepository;
import com.example.TesisRA.repository.EventoRepository;
import com.example.TesisRA.repository.EventoRepository;
import com.example.TesisRA.repository.Evento_AmbienteRepository;
import com.example.TesisRA.repository.HorarioRepository;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class Evento_AmbienteController {

    @Autowired
    Evento_AmbienteRepository eventoAmbienteRepository;
    @Autowired
    AmbienteRepository ambienteRepository;
    @Autowired
    EventoRepository eventoRepository;
    @Autowired
    HorarioRepository horarioRepository;

    //Obtener todos los eventoAmbientes
    @GetMapping("/eventoAmbientes")
    public List<Evento_Ambiente> getAllEvento_Ambientes() {
        return eventoAmbienteRepository.findAll();
    }

    // Crear un nuevo eventoAmbiente
    @PostMapping("/eventoAmbientes")
    public Evento_Ambiente createEvento_Ambiente(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject ambienteJSON = body.getJSONObject(0);
        JSONObject eventoJSON = body.getJSONObject(1);
        JSONArray horariosJSON = (JSONArray) body.get(2);
        //Lista de horarios a editar
        List<Horario> horariosRegistrados = new ArrayList<>();
        //Obtener el ambiente
        Ambiente ambiente = ambienteRepository.findById(ambienteJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteJSON.getLong("id")));
        //Obtener el evento
        Evento evento = eventoRepository.findById(eventoJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Evento", "id", eventoJSON.getLong("id")));

        //Verificar que el evento y el ambiente existan
        if (ambiente.getId() != null && evento.getId() != null) {
            //Crear el nuevo ambiente evento
            Evento_Ambiente eventoAmbiente = new Evento_Ambiente();
            eventoAmbiente.setAmbiente(ambiente);
            eventoAmbiente.setEvento(evento);
            eventoAmbiente.setHorarios(new ArrayList<>());

            //Barrer la lista de horarios y asignarla
            for (int i = 0; i < horariosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject horario = horariosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Horario horarioEnlazado = horarioRepository.findById(horario.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horario.getLong("id")));
                //Verificiar que exista el horario
                if (horarioEnlazado.getId() != null) {
                    //Añadir los horarios al ambiente evento    
                    eventoAmbiente.getHorarios().add(horarioEnlazado);
                }
            }

            //Crear el eventoAmbiente en base de datos
            Evento_Ambiente eventoAmbienteCreado = eventoAmbienteRepository.save(eventoAmbiente);

            //Asignar en nuevo ambiente evento a cada uno de los objetos iniciales
            //Ambiente
            //Verificar que exista la lista de ambientes evento sino crearla
            if (ambiente.getEsUsadoEventos() == null) {
                ambiente.setEsUsadoEventos(new ArrayList<>());
            }
            ambiente.getEsUsadoEventos().add(eventoAmbienteCreado);
            //Guardarlo en base de datos
            ambienteRepository.save(ambiente);

            //Evento
            if (evento.getEsRealizadoAmbientes() == null) {
                evento.setEsRealizadoAmbientes(new ArrayList<>());
            }
            evento.getEsRealizadoAmbientes().add(eventoAmbienteCreado);
            //Guardarlo en base de datos
            eventoRepository.save(evento);

            //Horarios
            //Barrer la lista de horarios y asignarla
            for (int i = 0; i < horariosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject horario = horariosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Horario horarioEnlazado = horarioRepository.findById(horario.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horario.getLong("id")));
                //Verificiar que exista el horario
                if (horarioEnlazado.getId() != null) {
                    //Añadir los horarios al ambiente evento    
                    horarioEnlazado.getEventos().add(eventoAmbienteCreado);
                    //Guardarlo en base de datos
                    horarioRepository.save(horarioEnlazado);
                }
            }
            return eventoAmbienteCreado;
        }
        return null;
    }

    // Obtener un eventoAmbiente unico
    @GetMapping("/eventoAmbientes/{id}")
    public Evento_Ambiente getEvento_AmbienteById(@PathVariable(value = "id") Long eventoAmbienteId) {
        return eventoAmbienteRepository.findById(eventoAmbienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Evento_Ambiente", "id", eventoAmbienteId));
    }

    // Editar un eventoAmbiente
    @PutMapping("/eventoAmbientes/{id}")
    public Evento_Ambiente updateEvento_Ambiente(@PathVariable(value = "id") Long eventoAmbienteId,
            @Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject ambienteJSON = body.getJSONObject(0);
        JSONObject eventoJSON = body.getJSONObject(1);
        JSONArray horariosJSON = (JSONArray) body.get(2);

        Evento_Ambiente eventoAmbiente = eventoAmbienteRepository.findById(eventoAmbienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Evento_Ambiente", "id", eventoAmbienteId));

        //Obtener el ambiente
        Ambiente ambiente = ambienteRepository.findById(ambienteJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteJSON.getLong("id")));
        //Obtener el evento
        Evento evento = eventoRepository.findById(eventoJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Evento", "id", eventoJSON.getLong("id")));

        //Verificar que el evento y el ambiente existan
        if (eventoAmbiente.getId() != null && ambiente.getId() != null && evento.getId() != null) {
            //Crear el nuevo ambiente evento

            eventoAmbiente.setAmbiente(ambiente);
            eventoAmbiente.setEvento(evento);

            //Eliminar los cursos de los horarios
            for (Horario horario : eventoAmbiente.getHorarios()) {
                horario.getEventos().remove(eventoAmbiente);
                horarioRepository.save(horario);
            }
            
            //Reinicializar
            eventoAmbiente.setHorarios(new ArrayList<>());

            //Barrer la lista de horarios y asignarla
            for (int i = 0; i < horariosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject horario = horariosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Horario horarioEnlazado = horarioRepository.findById(horario.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horario.getLong("id")));
                //Verificiar que exista el horario
                if (horarioEnlazado.getId() != null) {
                    //Añadir los horarios al ambiente evento    
                    eventoAmbiente.getHorarios().add(horarioEnlazado);
                }
            }

            //Editar el eventoAmbiente en base de datos
            Evento_Ambiente eventoAmbienteEditado = eventoAmbienteRepository.save(eventoAmbiente);

            //Asignar en nuevo ambiente evento a cada uno de los objetos iniciales
            //Ambiente
            //Verificar que exista la lista de ambientes evento sino crearla
            if (ambiente.getEsUsadoEventos() == null) {
                ambiente.setEsUsadoEventos(new ArrayList<>());
                ambiente.getEsUsadoEventos().add(eventoAmbienteEditado);
                //Guardarlo en base de datos
                ambienteRepository.save(ambiente);
            } else {
                //Verificar que no exista en la lista
                if (!ambiente.getEsUsadoEventos().contains(eventoAmbienteEditado)) {
                    ambiente.getEsUsadoEventos().add(eventoAmbienteEditado);
                    //Guardarlo en base de datos
                    ambienteRepository.save(ambiente);
                }
            }
            //Evento
            if (evento.getEsRealizadoAmbientes() == null) {
                evento.setEsRealizadoAmbientes(new ArrayList<>());
                evento.getEsRealizadoAmbientes().add(eventoAmbienteEditado);
                //Guardarlo en base de datos
                eventoRepository.save(evento);
            } else {
                if (!evento.getEsRealizadoAmbientes().contains(eventoAmbienteEditado)) {
                    evento.getEsRealizadoAmbientes().add(eventoAmbienteEditado);
                    //Guardarlo en base de datos
                    eventoRepository.save(evento);
                }
            }

            //Horarios
            //Barrer la lista de horarios y asignarla
            for (int i = 0; i < horariosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject horario = horariosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Horario horarioEnlazado = horarioRepository.findById(horario.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Horario", "id", horario.getLong("id")));
                //Verificiar que exista el horario
                if (horarioEnlazado.getId() != null) {
                    if (horarioEnlazado.getEventos() != null) {
                        if (!horarioEnlazado.getEventos().contains(eventoAmbienteEditado)) {
                            //Añadir los horarios al ambiente evento    
                            horarioEnlazado.getEventos().add(eventoAmbienteEditado);
                            //Guardarlo en base de datos
                            horarioRepository.save(horarioEnlazado);
                        }
                    }
                }
            }
            return eventoAmbienteEditado;
        }
        return null;
    }

    // Eliminar un eventoAmbiente
    @DeleteMapping("/eventoAmbientes/{id}")
    public ResponseEntity<?> deleteEvento_Ambiente(@PathVariable(value = "id") Long eventoAmbienteId) {
        Evento_Ambiente eventoAmbiente = eventoAmbienteRepository.findById(eventoAmbienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Evento_Ambiente", "id", eventoAmbienteId));

        eventoAmbienteRepository.delete(eventoAmbiente);

        return ResponseEntity.ok().build();
    }
}
