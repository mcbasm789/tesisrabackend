/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Docente;
import com.example.TesisRA.repository.DocenteRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class DocenteController {

    @Autowired
    DocenteRepository docenteRepository;

    //Obtener todos los docentes
    @GetMapping("/docentes")
    public List<Docente> getAllDocentes() {
        return docenteRepository.findAll();
    }

    // Crear un nuevo docente
    @PostMapping("/docentes")
    public Docente createDocente(@Valid @RequestBody Docente docente) {
        return docenteRepository.save(docente);
    }

    // Obtener un docente unico
    @GetMapping("/docentes/{id}")
    public Docente getDocenteById(@PathVariable(value = "id") Long docenteId) {
        return docenteRepository.findById(docenteId)
                .orElseThrow(() -> new ResourceNotFoundException("Docente", "id", docenteId));
    }

    // Editar un docente
    @PutMapping("/docentes/{id}")
    public Docente updateDocente(@PathVariable(value = "id") Long docenteId,
            @Valid @RequestBody Docente docenteDetails) {

        Docente docente = docenteRepository.findById(docenteId)
                .orElseThrow(() -> new ResourceNotFoundException("Docente", "id", docenteId));

        docente.setApPaterno(docenteDetails.getApPaterno());
        docente.setApMaterno(docenteDetails.getApMaterno());
        docente.setNombre(docenteDetails.getNombre());
        docente.setDictaCursos(docenteDetails.getDictaCursos());

        Docente docenteEditado = docenteRepository.save(docente);
        return docenteEditado;
    }

    // Eliminar un docente
    @DeleteMapping("/docentes/{id}")
    public ResponseEntity<?> deleteDocente(@PathVariable(value = "id") Long docenteId) {
        Docente docente = docenteRepository.findById(docenteId)
                .orElseThrow(() -> new ResourceNotFoundException("Docente", "id", docenteId));

        docenteRepository.delete(docente);

        return ResponseEntity.ok().build();
    }
}
