/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Tipo_Ambiente;
import com.example.TesisRA.repository.Tipo_AmbienteRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class Tipo_AmbienteController {

    @Autowired
    Tipo_AmbienteRepository tipoAmbienteRepository;

    //Obtener todos los tipoAmbientes
    @GetMapping("/tipoAmbientes")
    public List<Tipo_Ambiente> getAllTipo_Ambientes() {
        return tipoAmbienteRepository.findAll();
    }

    // Crear un nuevo tipoAmbiente
    @PostMapping("/tipoAmbientes")
    public Tipo_Ambiente createTipo_Ambiente(@Valid @RequestBody Tipo_Ambiente tipoAmbiente) {
        return tipoAmbienteRepository.save(tipoAmbiente);
    }

    // Obtener un tipoAmbiente unico
    @GetMapping("/tipoAmbientes/{id}")
    public Tipo_Ambiente getTipo_AmbienteById(@PathVariable(value = "id") Long tipoAmbienteId) {
        return tipoAmbienteRepository.findById(tipoAmbienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Tipo_Ambiente", "id", tipoAmbienteId));
    }

    // Editar un tipoAmbiente
    @PutMapping("/tipoAmbientes/{id}")
    public Tipo_Ambiente updateTipo_Ambiente(@PathVariable(value = "id") Long tipoAmbienteId,
            @Valid @RequestBody Tipo_Ambiente tipoAmbienteDetails) {

        Tipo_Ambiente tipoAmbiente = tipoAmbienteRepository.findById(tipoAmbienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Tipo_Ambiente", "id", tipoAmbienteId));

        tipoAmbiente.setNombre(tipoAmbienteDetails.getNombre());
        tipoAmbiente.setRegistraAmbientes(tipoAmbienteDetails.getRegistraAmbientes());

        Tipo_Ambiente tipoAmbienteEditado = tipoAmbienteRepository.save(tipoAmbiente);
        return tipoAmbienteEditado;
    }

    // Eliminar un tipoAmbiente
    @DeleteMapping("/tipoAmbientes/{id}")
    public ResponseEntity<?> deleteTipo_Ambiente(@PathVariable(value = "id") Long tipoAmbienteId) {
        Tipo_Ambiente tipoAmbiente = tipoAmbienteRepository.findById(tipoAmbienteId)
                .orElseThrow(() -> new ResourceNotFoundException("Tipo_Ambiente", "id", tipoAmbienteId));

        tipoAmbienteRepository.delete(tipoAmbiente);

        return ResponseEntity.ok().build();
    }
}
