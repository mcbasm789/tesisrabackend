/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Alumno;
import com.example.TesisRA.repository.AlumnoRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class AlumnoController {

    @Autowired
    AlumnoRepository alumnoRepository;

    //Obtener todos los alumnos
    @GetMapping("/alumnos")
    public List<Alumno> getAllAlumnos() {
        return alumnoRepository.findAll();
    }

    // Crear un nuevo alumno
    @PostMapping("/alumnos")
    public Alumno createAlumno(@Valid @RequestBody Alumno alumno) {
        return alumnoRepository.save(alumno);
    }

    // Obtener un alumno unico
    @GetMapping("/alumnos/{id}")
    public Alumno getAlumnoById(@PathVariable(value = "id") Long alumnoId) {
        return alumnoRepository.findById(alumnoId)
                .orElseThrow(() -> new ResourceNotFoundException("Alumno", "id", alumnoId));
    }

    // Editar un alumno
    @PutMapping("/alumnos/{id}")
    public Alumno updateAlumno(@PathVariable(value = "id") Long alumnoId,
            @Valid @RequestBody Alumno alumnoDetails) {

        Alumno alumno = alumnoRepository.findById(alumnoId)
                .orElseThrow(() -> new ResourceNotFoundException("Alumno", "id", alumnoId));

        alumno.setApPaterno(alumnoDetails.getApPaterno());
        alumno.setApMaterno(alumnoDetails.getApMaterno());
        alumno.setCodigo(alumnoDetails.getCodigo());
        alumno.setEstado(alumnoDetails.getEstado());
        alumno.setNombre(alumnoDetails.getNombre());
        alumno.setLlevaCursos(alumnoDetails.getLlevaCursos());

        Alumno alumnoEditado = alumnoRepository.save(alumno);
        return alumnoEditado;
    }

    // Eliminar un alumno
    @DeleteMapping("/alumnos/{id}")
    public ResponseEntity<?> deleteAlumno(@PathVariable(value = "id") Long alumnoId) {
        Alumno alumno = alumnoRepository.findById(alumnoId)
                .orElseThrow(() -> new ResourceNotFoundException("Alumno", "id", alumnoId));

        alumnoRepository.delete(alumno);

        return ResponseEntity.ok().build();
    }
}
