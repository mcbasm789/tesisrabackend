/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Evento;
import com.example.TesisRA.repository.EventoRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class EventoController {
    
    @Autowired
    EventoRepository eventoRepository;

    //Obtener todos los eventos
    @GetMapping("/eventos")
    public List<Evento> getAllEventos() {
        return eventoRepository.findAll();
    }

    // Crear un nuevo evento
    @PostMapping("/eventos")
    public Evento createEvento(@Valid @RequestBody Evento evento) {
        return eventoRepository.save(evento);
    }

    // Obtener un evento unico
    @GetMapping("/eventos/{id}")
    public Evento getEventoById(@PathVariable(value = "id") Long eventoId) {
        return eventoRepository.findById(eventoId)
                .orElseThrow(() -> new ResourceNotFoundException("Evento", "id", eventoId));
    }

    // Editar un evento
    @PutMapping("/eventos/{id}")
    public Evento updateEvento(@PathVariable(value = "id") Long eventoId,
            @Valid @RequestBody Evento eventoDetails) {
        
        Evento evento = eventoRepository.findById(eventoId)
                .orElseThrow(() -> new ResourceNotFoundException("Evento", "id", eventoId));
        
        evento.setAforo(eventoDetails.getAforo());
        evento.setNombre(eventoDetails.getNombre());
        evento.setFecha(eventoDetails.getFecha());
        evento.setRealizado(eventoDetails.getRealizado());
        evento.setExpositor(eventoDetails.getExpositor());
        evento.setEsRealizadoAmbientes(eventoDetails.getEsRealizadoAmbientes());
        
        Evento eventoEditado = eventoRepository.save(evento);
        return eventoEditado;
    }

    // Eliminar un evento
    @DeleteMapping("/eventos/{id}")
    public ResponseEntity<?> deleteEvento(@PathVariable(value = "id") Long eventoId) {
        Evento evento = eventoRepository.findById(eventoId)
                .orElseThrow(() -> new ResourceNotFoundException("Evento", "id", eventoId));
        
        eventoRepository.delete(evento);
        
        return ResponseEntity.ok().build();
    }
}
