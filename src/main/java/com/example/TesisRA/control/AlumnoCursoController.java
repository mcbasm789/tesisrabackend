/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Alumno;
import com.example.TesisRA.modelo.Curso;
import com.example.TesisRA.repository.AlumnoRepository;
import com.example.TesisRA.repository.CursoRepository;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class AlumnoCursoController {

    @Autowired
    AlumnoRepository alumnoRepository;
    @Autowired
    CursoRepository cursoRepository;

    // Crear un nuevo alumno
    @PostMapping("/asignarCurso")
    public Curso asignarCurso(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject cursoJSON = body.getJSONObject(0);
        JSONArray alumnosJSON = (JSONArray) body.get(1);
        //Lista de alumnos a editar
        List<Alumno> alumnosMatriculados = new ArrayList<>();
        //Obtener el curso
        Curso curso = cursoRepository.findById(cursoJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Curso", "id", cursoJSON.getLong("id")));
        //Verificar que exista el curso
        if (curso.getId() != null) //Barrer la lista de alumnos y asignarla
        {
            for (int i = 0; i < alumnosJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject alumno = alumnosJSON.getJSONObject(i);
                //Encontrar el alumno en la base de datos
                Alumno alumnoEnlazado = alumnoRepository.findById(alumno.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Alumno", "id", alumno.getLong("id")));
                //Verificiar que exista el alumno
                if (alumnoEnlazado.getId() != null) {
                    //Verificar que el alumno no este en el curso
                    Boolean llevaCurso = false;
                    //Si la lista no existe crearla
                    if (curso.getEsLlevadoAlumnos() == null) {
                        curso.setEsLlevadoAlumnos(new ArrayList<>());
                    } else {
                        for (Alumno esLlevadoAlumno : curso.getEsLlevadoAlumnos()) {
                            if (alumnoEnlazado == esLlevadoAlumno) {
                                llevaCurso = true;
                                break;
                            }
                        }
                    }
                    //Si el alumno no lleva el curso añadirlo al curso
                    if (!llevaCurso) {
                        curso.getEsLlevadoAlumnos().add(alumnoEnlazado);
                    }

                    //Verificar que el alumno no tenga registrado el curso para añadirlo
                    Boolean matriculadoCurso = false;
                    if (alumnoEnlazado.getLlevaCursos() == null) {
                        alumnoEnlazado.setLlevaCursos(new ArrayList<>());
                    } else {
                        for (Curso cursoMatriculado : alumnoEnlazado.getLlevaCursos()) {
                            if (curso == cursoMatriculado) {
                                matriculadoCurso = true;
                                break;
                            }
                        }
                    }
                    //Si no esta matriculado añadir el curso a su lista
                    if (!matriculadoCurso) {
                        alumnoEnlazado.getLlevaCursos().add(curso);
                        //Añadirlo a la lista de alumnos matriculados a editar
                        alumnosMatriculados.add(alumnoEnlazado);
                    }
                }
            }

            //Editar el curso y los alumnos;
            cursoRepository.save(curso);
            alumnosMatriculados.forEach((alumnoMatriculado) -> {
                alumnoRepository.save(alumnoMatriculado);
            });
        }
        return curso;
    }
}
