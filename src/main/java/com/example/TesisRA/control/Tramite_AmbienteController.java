package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Ambiente;
import com.example.TesisRA.modelo.Tramite_Administrativo;
import com.example.TesisRA.repository.AmbienteRepository;
import com.example.TesisRA.repository.Tramite_AdministrativoRepository;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class Tramite_AmbienteController {

    @Autowired
    Tramite_AdministrativoRepository tramiteAdministrativoRepository;

    @Autowired
    AmbienteRepository ambienteRepository;

    // Crear un nuevo alumno
    @PostMapping("/asignarTramites")
    public Ambiente asignarTramites(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject ambienteJSON = body.getJSONObject(0);
        JSONArray tramitesJSON = (JSONArray) body.get(1);
        //Lista de tramites a editar
        List<Tramite_Administrativo> tramitesRegistrados = new ArrayList<>();
        //Obtener el ambiente
        Ambiente ambiente = ambienteRepository.findById(ambienteJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Ambiente", "id", ambienteJSON.getLong("id")));
        //Verificar que exista el ambiente
        if (ambiente.getId() != null) //Barrer la lista de tramites y asignarla
        {
            for (int i = 0; i < tramitesJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject tramite = tramitesJSON.getJSONObject(i);
                //Encontrar el tramite en la base de datos
                Tramite_Administrativo tramiteEnlazado = tramiteAdministrativoRepository.findById(tramite.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Tramite Administrativo", "id", tramite.getLong("id")));
                //Verificiar que exista el tramite
                if (tramiteEnlazado.getId() != null) {
                    //Verificar que el tramite no este en el ambiente
                    Boolean realizaTramite = false;
                    //Si la lista no existe crearla
                    if (ambiente.getRealizaTramites() == null) {
                        ambiente.setRealizaTramites(new ArrayList<Tramite_Administrativo>());
                    } else {
                        for (Tramite_Administrativo tramiteRealizado : ambiente.getRealizaTramites()) {
                            if (tramiteEnlazado == tramiteRealizado) {
                                realizaTramite = true;
                                break;
                            }
                        }
                    }
                    //Si el ambiente no realiza el tramite añadirlo al tramite
                    if (!realizaTramite) {
                        ambiente.getRealizaTramites().add(tramiteEnlazado);
                    }

                    //Verificar que el tramite no tenga registrado el ambiente para añadirlo
                    Boolean realizadaOficina = false;
                    if (tramiteEnlazado.getEsRealizadaOficinas() == null) {
                        tramiteEnlazado.setEsRealizadaOficinas(new ArrayList<Ambiente>());
                    } else {
                        for (Ambiente ambienteRealizado : tramiteEnlazado.getEsRealizadaOficinas()) {
                            if (ambiente == ambienteRealizado) {
                                realizadaOficina = true;
                                break;
                            }
                        }
                    }
                    //Si no esta matriculado añadir el curso a su lista
                    if (!realizadaOficina) {
                        tramiteEnlazado.getEsRealizadaOficinas().add(ambiente);
                        //Añadirlo a la lista de alumnos matriculados a editar
                        tramitesRegistrados.add(tramiteEnlazado);
                    }
                }
            }

            //Editar el ambiente y los tramites;
            ambienteRepository.save(ambiente);
            tramitesRegistrados.forEach((tramiteRegistrado) -> {
                tramiteAdministrativoRepository.save(tramiteRegistrado);
            });
        }
        return ambiente;
    }
}
