/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Facultad;
import com.example.TesisRA.repository.FacultadRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class FacultadController {

    @Autowired
    FacultadRepository facultadRepository;

    //Obtener todos los facultads
    @GetMapping("/facultades")
    public List<Facultad> getAllFacultads() {
        return facultadRepository.findAll();
    }

    // Crear un nuevo facultad
    @PostMapping("/facultades")
    public Facultad createFacultad(@Valid @RequestBody Facultad facultad) {
        return facultadRepository.save(facultad);
    }

    // Obtener un facultad unico
    @GetMapping("/facultades/{id}")
    public Facultad getFacultadById(@PathVariable(value = "id") Long facultadId) {
        return facultadRepository.findById(facultadId)
                .orElseThrow(() -> new ResourceNotFoundException("Facultad", "id", facultadId));
    }

    // Editar un facultad
    @PutMapping("/facultades/{id}")
    public Facultad updateFacultad(@PathVariable(value = "id") Long facultadId,
            @Valid @RequestBody Facultad facultadDetails) {

        Facultad facultad = facultadRepository.findById(facultadId)
                .orElseThrow(() -> new ResourceNotFoundException("Facultad", "id", facultadId));

        facultad.setNombre(facultadDetails.getNombre());
        facultad.setGestionaEscuelas(facultadDetails.getGestionaEscuelas());

        Facultad facultadEditado = facultadRepository.save(facultad);
        return facultadEditado;
    }

    // Eliminar un facultad
    @DeleteMapping("/facultades/{id}")
    public ResponseEntity<?> deleteFacultad(@PathVariable(value = "id") Long facultadId) {
        Facultad facultad = facultadRepository.findById(facultadId)
                .orElseThrow(() -> new ResourceNotFoundException("Facultad", "id", facultadId));

        facultadRepository.delete(facultad);

        return ResponseEntity.ok().build();
    }
}
