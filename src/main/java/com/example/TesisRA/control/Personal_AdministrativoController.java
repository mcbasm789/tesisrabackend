/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Cargo;
import com.example.TesisRA.modelo.Escuela_Profesional;
import com.example.TesisRA.modelo.Facultad;
import com.example.TesisRA.modelo.Personal_Administrativo;
import com.example.TesisRA.repository.CargoRepository;
import com.example.TesisRA.repository.Personal_AdministrativoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class Personal_AdministrativoController {

    @Autowired
    Personal_AdministrativoRepository personalAdminsitrativoRepository;
    @Autowired
    CargoRepository cargoRepository;

    //Obtener todos los personalAdminsitrativos
    @GetMapping("/personalAdminsitrativo")
    public List<Personal_Administrativo> getAllPersonal_Administrativos() {
        return personalAdminsitrativoRepository.findAll();
    }

    // Crear un nuevo personalAdminsitrativo
    @PostMapping("/personalAdminsitrativo")
    public Personal_Administrativo createPersonal_Administrativo(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject personalJSON = body.getJSONObject(0);
        JSONObject cargoJSON = body.getJSONObject(1);

        //Obtener la facultad
        Cargo cargo = cargoRepository.findById(cargoJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Cargo", "id", cargoJSON.getLong("id")));
        //Verificar la existencia de la lista de escuelas
        if (cargo.getId() != null) {
            try {
                //Si no existe crearla
                if (cargo.getDesignaPersonal() == null) {
                    cargo.setDesignaPersonal(new ArrayList<Personal_Administrativo>());
                }
                //Crear la escuela profesional desde el JSON
                Personal_Administrativo personal_Administrativo = new ObjectMapper().readValue(personalJSON.toString(), Personal_Administrativo.class);
                //Asignar la facultad
                personal_Administrativo.setCargo(cargo);
                //Crearla en base de datos
                Personal_Administrativo personalCreado = personalAdminsitrativoRepository.save(personal_Administrativo);

                //Asignar la escuela creada a la facultad
                cargo.getDesignaPersonal().add(personalCreado);
                //Editar la facultad
                cargoRepository.save(cargo);
                return personalCreado;
            } catch (IOException ex) {
                Logger.getLogger(Escuela_ProfesionalController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    // Obtener un personalAdminsitrativo unico
    @GetMapping("/personalAdminsitrativo/{id}")
    public Personal_Administrativo getPersonal_AdministrativoById(@PathVariable(value = "id") Long personalAdminsitrativoId) {
        return personalAdminsitrativoRepository.findById(personalAdminsitrativoId)
                .orElseThrow(() -> new ResourceNotFoundException("Personal_Administrativo", "id", personalAdminsitrativoId));
    }

    // Editar un personalAdminsitrativo
    @PutMapping("/personalAdminsitrativo/{id}")
    public Personal_Administrativo updatePersonal_Administrativo(@PathVariable(value = "id") Long personalAdminsitrativoId,
            @Valid @RequestBody Object[] requestBody) {

        Personal_Administrativo personalAdminsitrativo = personalAdminsitrativoRepository.findById(personalAdminsitrativoId)
                .orElseThrow(() -> new ResourceNotFoundException("Personal_Administrativo", "id", personalAdminsitrativoId));

        if (personalAdminsitrativo.getId() != null) {
            //Dividir el arreglo
            JSONArray body = new JSONArray(requestBody);
            JSONObject personalJSON = body.getJSONObject(0);
            JSONObject cargoJSON = body.getJSONObject(1);

            //Obtener la facultad
            Cargo cargo = cargoRepository.findById(cargoJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Cargo", "id", cargoJSON.getLong("id")));
            if (cargo.getId() != null) {
                try {

                    //Crear la escuela profesional desde el JSON
                    Personal_Administrativo personalAdministrativoDetails = new ObjectMapper().readValue(personalJSON.toString(), Personal_Administrativo.class);
                    //Asignar la facultad
                    personalAdminsitrativo.setApPaterno(personalAdministrativoDetails.getApPaterno());
                    personalAdminsitrativo.setApMaterno(personalAdministrativoDetails.getApMaterno());
                    personalAdminsitrativo.setCargo(personalAdministrativoDetails.getCargo());
                    personalAdminsitrativo.setAmbiente(personalAdministrativoDetails.getAmbiente());
                    personalAdminsitrativo.setNombre(personalAdministrativoDetails.getNombre());
                    personalAdminsitrativo.setAmbiente(personalAdministrativoDetails.getAmbiente());
                    personalAdminsitrativo.setCargo(cargo);

                    Personal_Administrativo personalEditado = personalAdminsitrativoRepository.save(personalAdminsitrativo);

                    //Verificar si la facultad ya gestiona dicha escuela
                    Boolean designaPersonal = false;

                    //Verificar la existencia de la lista de escuelas
                    if (cargo.getDesignaPersonal() == null) {
                        //Si no existe crearla
                        cargo.setDesignaPersonal(new ArrayList<Personal_Administrativo>());
                    } else {
                        //Verificar que la escuela no este en la facultad
                        for (Personal_Administrativo personal : cargo.getDesignaPersonal()) {
                            if (personal == personalEditado) {
                                designaPersonal = true;
                                break;
                            }
                        }
                    }
                    //Asignar la escuela creada a la facultad (si es necesario)
                    if (!designaPersonal) {
                        cargo.getDesignaPersonal().add(personalEditado);
                        //Editar la facultad
                        cargoRepository.save(cargo);
                    }
                    return personalEditado;
                } catch (IOException ex) {
                    Logger.getLogger(Personal_AdministrativoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    // Eliminar un personalAdminsitrativo
    @DeleteMapping("/personalAdminsitrativo/{id}")
    public ResponseEntity<?> deletePersonal_Administrativo(@PathVariable(value = "id") Long personalAdminsitrativoId) {
        Personal_Administrativo personalAdminsitrativo = personalAdminsitrativoRepository.findById(personalAdminsitrativoId)
                .orElseThrow(() -> new ResourceNotFoundException("Personal_Administrativo", "id", personalAdminsitrativoId));

        personalAdminsitrativoRepository.delete(personalAdminsitrativo);

        return ResponseEntity.ok().build();
    }
}
