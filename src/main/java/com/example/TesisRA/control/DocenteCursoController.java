/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.control;

import com.example.TesisRA.exception.ResourceNotFoundException;
import com.example.TesisRA.modelo.Alumno;
import com.example.TesisRA.modelo.Curso;
import com.example.TesisRA.modelo.Docente;
import com.example.TesisRA.repository.CursoRepository;
import com.example.TesisRA.repository.DocenteRepository;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SebastianM
 */
@RestController
@RequestMapping("/api")
public class DocenteCursoController {

    @Autowired
    CursoRepository cursoRepository;
    @Autowired
    DocenteRepository docenteRepository;

    // Crear un nuevo alumno
    @PostMapping("/asignarCursoDocente")
    public Curso asignarCursoDocente(@Valid @RequestBody Object[] requestBody) {
        //Dividir el arreglo
        JSONArray body = new JSONArray(requestBody);
        JSONObject cursoJSON = body.getJSONObject(0);
        JSONArray docentesJSON = (JSONArray) body.get(1);
        //Lista de docnetes a editar
        List<Docente> docentesMatriculados = new ArrayList<>();
        //Obtener el curso
        Curso curso = cursoRepository.findById(cursoJSON.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Curso", "id", cursoJSON.getLong("id")));
        //Verificar que exista el curso
        if (curso.getId() != null) //Barrer la lista de docentes y asignarla
        {
            for (int i = 0; i < docentesJSON.length(); i++) {
                //Objeto a ser evaluado
                JSONObject docente = docentesJSON.getJSONObject(i);
                //Encontrar el docente en la base de datos
                Docente docenteEnlazado = docenteRepository.findById(docente.getLong("id")).orElseThrow(() -> new ResourceNotFoundException("Docente", "id", cursoJSON.getLong("id")));
                //Verificiar que exista el docente
                if (docenteEnlazado.getId() != null) {
                    //Verificar que el docente no este en el curso
                    Boolean llevaCurso = false;
                    //Si la lista no existe crearla
                    if (curso.getEsDictadoDocentes() == null) {
                        curso.setEsDictadoDocentes(new ArrayList<>());
                    } else {
                        for (Docente esDictadoDocente : curso.getEsDictadoDocentes()) {
                            if (docenteEnlazado == esDictadoDocente) {
                                llevaCurso = true;
                                break;
                            }
                        }
                    }
                    //Si el docente no lleva el curso añadirlo al curso
                    if (!llevaCurso) {
                        curso.getEsDictadoDocentes().add(docenteEnlazado);
                    }

                    //Verificar que el docente no tenga registrado el curso para añadirlo
                    Boolean matriculadoCurso = false;
                    if (docenteEnlazado.getDictaCursos() == null) {
                        docenteEnlazado.setDictaCursos(new ArrayList<>());
                    } else {
                        for (Curso cursoMatriculado : docenteEnlazado.getDictaCursos()) {
                            if (curso == cursoMatriculado) {
                                matriculadoCurso = true;
                                break;
                            }
                        }
                    }
                    //Si no esta matriculado añadir el curso a su lista
                    if (!matriculadoCurso) {
                        docenteEnlazado.getDictaCursos().add(curso);
                        //Añadirlo a la lista de docentes matriculados a editar
                        docentesMatriculados.add(docenteEnlazado);
                    }
                }
            }

            //Editar el curso y los docentes;
            cursoRepository.save(curso);
            docentesMatriculados.forEach((docenteMatriculado) -> {
                docenteRepository.save(docenteMatriculado);
            });
        }
        return curso;
    }
}
