/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.repository;

import com.example.TesisRA.modelo.Horario;
import com.example.TesisRA.modelo.Tipo_Ambiente;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author SebastianM
 */
@Repository
public interface HorarioRepository extends JpaRepository<Horario, Long> {

    @Query("SELECT h FROM Horario h WHERE h.dia = :dia ORDER BY h.horaInicio")
    public List<Horario> findByDia(@Param("dia") String dia);
}
