/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.repository;

import com.example.TesisRA.modelo.Tipo_Ambiente;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author SebastianM
 */
@Repository
public interface Tipo_AmbienteRepository extends JpaRepository<Tipo_Ambiente, Long> {

    @Query("SELECT ta FROM Tipo_Ambiente ta WHERE ta.nombre = :nombre")
    public Tipo_Ambiente findByNombre(@Param("nombre") String nombre);
}
