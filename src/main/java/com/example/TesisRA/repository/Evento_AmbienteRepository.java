/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.repository;

import com.example.TesisRA.modelo.Evento_Ambiente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author SebastianM
 */
@Repository
public interface Evento_AmbienteRepository extends JpaRepository<Evento_Ambiente, Long> {

}
