/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.repository;

import com.example.TesisRA.modelo.Ambiente;
import com.example.TesisRA.modelo.Tipo_Ambiente;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author SebastianM
 */
@Repository
public interface AmbienteRepository extends JpaRepository<Ambiente, Long> {

    //Obtener los ambientes de un determinado tipo
    @Query("SELECT a FROM Ambiente a WHERE a.tipoAmbiente = :tipoAmbiente")
    public List<Ambiente> findByTipo(@Param("tipoAmbiente") Tipo_Ambiente tipoAmbiente);

    //Obtener los ambientes que no sean de un tipo
    @Query("SELECT a FROM Ambiente a WHERE a.tipoAmbiente NOT LIKE :tipoAmbiente")
    public List<Ambiente> findByNotTipo(@Param("tipoAmbiente") Tipo_Ambiente tipoAmbiente);

    //Obtener los ambientes por su idVuforia
    @Query("SELECT a FROM Ambiente a WHERE a.idVuforia  LIKE :idVuforia")
    public Ambiente findByIdVuforia(@Param("idVuforia") String idVuforia);
}
