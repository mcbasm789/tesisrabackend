/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author SebastianM
 */
@Entity
@Table(name = "Cursos")
@EntityListeners(AuditingEntityListener.class)
public class Curso implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String nombre;
    @NotBlank
    private String codigo;

    //Lista de alumnos
    @ManyToMany(mappedBy = "llevaCursos")
    @JsonIgnoreProperties("llevaCursos")
    private List<Alumno> esLlevadoAlumnos;

    //Lista de Docentes
    @ManyToMany(mappedBy = "dictaCursos")
    @JsonIgnoreProperties("dictaCursos")
    private List<Docente> esDictadoDocentes;

    //Lista de Ambientes
    @OneToMany(mappedBy = "curso")
    @JsonIgnoreProperties("curso")
    private List<Ambiente_Curso> esDictadoAmbientes;

    //Escuela por la que es dictada
    @ManyToOne
    @JsonIgnoreProperties("gestionaCursos")
    private Escuela_Profesional escuelaProfesional;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<Ambiente_Curso> getEsDictadoAmbientes() {
        return esDictadoAmbientes;
    }

    public void setEsDictadoAmbientes(List<Ambiente_Curso> esDictadoAmbientes) {
        this.esDictadoAmbientes = esDictadoAmbientes;
    }

    public Escuela_Profesional getEscuelaProfesional() {
        return escuelaProfesional;
    }

    public void setEscuelaProfesional(Escuela_Profesional escuelaProfesional) {
        this.escuelaProfesional = escuelaProfesional;
    }

    public List<Alumno> getEsLlevadoAlumnos() {
        return esLlevadoAlumnos;
    }

    public void setEsLlevadoAlumnos(List<Alumno> esLlevadoAlumnos) {
        this.esLlevadoAlumnos = esLlevadoAlumnos;
    }

    public List<Docente> getEsDictadoDocentes() {
        return esDictadoDocentes;
    }

    public void setEsDictadoDocentes(List<Docente> esDictadoDocentes) {
        this.esDictadoDocentes = esDictadoDocentes;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
