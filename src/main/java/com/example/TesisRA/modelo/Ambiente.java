/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author SebastianM
 */
@Entity
@Table(name = "Ambientes")
@EntityListeners(AuditingEntityListener.class)
public class Ambiente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String nombre;
    @NotBlank
    private String codigo;
    private Integer aforo;
    //Valores del Marcador
    private String idVuforia;
    private String urlImagen;
    private Integer nroPiso;

    //Lista de Cursos
    @OneToMany(mappedBy = "ambiente")
    @JsonIgnoreProperties("ambiente")
    private List<Ambiente_Curso> esUsadoCursos;

    //Lista de Eventos
    @OneToMany(mappedBy = "ambiente")
    @JsonIgnoreProperties("ambiente")
    private List<Evento_Ambiente> esUsadoEventos;

    //Lista de Tramites
    @ManyToMany(mappedBy = "esRealizadaOficinas")
    @JsonIgnoreProperties("esRealizadaOficinas")
    private List<Tramite_Administrativo> realizaTramites;

    //Personal Asignado
    @OneToMany(mappedBy = "ambiente")
    @JsonIgnoreProperties("ambiente")
    private List<Personal_Administrativo> gestionaPersonal;

    //Tipo de Ambiente
    @ManyToOne
    @JsonIgnoreProperties("registraAmbientes")
    private Tipo_Ambiente tipoAmbiente;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    //Edificio al que pertenece
    @ManyToOne
    @JsonIgnoreProperties("contieneAmbientes")
    private Edificio edificio;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getAforo() {
        return aforo;
    }

    public void setAforo(Integer aforo) {
        this.aforo = aforo;
    }

    public String getIdVuforia() {
        return idVuforia;
    }

    public void setIdVuforia(String idVuforia) {
        this.idVuforia = idVuforia;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public Integer getNroPiso() {
        return nroPiso;
    }

    public void setNroPiso(Integer nroPiso) {
        this.nroPiso = nroPiso;
    }

    public List<Ambiente_Curso> getEsUsadoCursos() {
        return esUsadoCursos;
    }

    public void setEsUsadoCursos(List<Ambiente_Curso> esUsadoCursos) {
        this.esUsadoCursos = esUsadoCursos;
    }

    public List<Tramite_Administrativo> getRealizaTramites() {
        return realizaTramites;
    }

    public void setRealizaTramites(List<Tramite_Administrativo> realizaTramites) {
        this.realizaTramites = realizaTramites;
    }

    public List<Personal_Administrativo> getGestionaPersonal() {
        return gestionaPersonal;
    }

    public void setGestionaPersonal(List<Personal_Administrativo> gestionaPersonal) {
        this.gestionaPersonal = gestionaPersonal;
    }

    public Tipo_Ambiente getTipoAmbiente() {
        return tipoAmbiente;
    }

    public void setTipoAmbiente(Tipo_Ambiente tipoAmbiente) {
        this.tipoAmbiente = tipoAmbiente;
    }

    public Edificio getEdificio() {
        return edificio;
    }

    public void setEdificio(Edificio edificio) {
        this.edificio = edificio;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Evento_Ambiente> getEsUsadoEventos() {
        return esUsadoEventos;
    }

    public void setEsUsadoEventos(List<Evento_Ambiente> esUsadoEventos) {
        this.esUsadoEventos = esUsadoEventos;
    }

}
