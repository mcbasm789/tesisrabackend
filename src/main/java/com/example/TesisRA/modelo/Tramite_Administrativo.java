/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.TesisRA.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author SebastianM
 */
@Entity
@Table(name = "Tramite_Administrativo")
@EntityListeners(AuditingEntityListener.class)
public class Tramite_Administrativo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String nombre;
    @Size(max = 10485760)
    @NotBlank
    private String descripcion;
    @NotBlank
    private String codigo;

    @ManyToMany
    @JsonIgnoreProperties({"realizaTramites", "esUsadoCursos", "esUsadoEventos"})
    private List<Ambiente> esRealizadaOficinas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<Ambiente> getEsRealizadaOficinas() {
        return esRealizadaOficinas;
    }

    public void setEsRealizadaOficinas(List<Ambiente> esRealizadaOficinas) {
        this.esRealizadaOficinas = esRealizadaOficinas;
    }

}
